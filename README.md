XML Schemas associated with Noark 5 Tjenestegrensesnitt
=======================================================

More information on the Noark 5 Tjenestegrensesnitt spesification can be found on
[the pages of The National Archives of Norway](https://www.arkivverket.no/forvaltning-og-utvikling/noark-standarden/noark-5/tjenestegrensesnitt-noark5).


Formatting
----------
All XML files are stored after converting to using unix line endings
with 'fromdos' and are formatted using xmllint like this:

  for x in $(find . -name '*.xsd'); do \
    xmllint --format "$x" > "$x.new" && mv "$x.new" "$x"; \
  done
